// function App() {
//     return (
//         <h1>Hello, React!</h1>
//     )
// }

import React from "react";

const Title = () => {
    return (
        <h1>Exemplo Eventos e State</h1>
    )
}

class App extends React.Component {

    constructor(props) {
        super(props)
        //this.updateCounter = this.updateCounter.bind(this)
        
        this.state = {
            counter: 0,
            mult: 1
        }
    }

    updateCounter = () => {
        console.log('Vai alterar o counter')

        this.setState({
            counter: this.state.counter + 1
        })
    }

    updateMult = () => {
        console.log('Vai alterar o mult')

        this.setState({
            mult: this.state.mult * 2
        })
    }

    getCounterOdd() {
        return this.state.counter % 2 === 0 ? 'Par' : 'Ímpar'
    }

    render() {
        return (
            <div>
                <Title />
                <p>
                    {this.props.message}
                    <br />
                    Contador: {this.state.counter} - {this.getCounterOdd()}<br />
                    <input type="button" value="Alterar Contador" onClick={this.updateCounter} />
                    <br /><br />
                    Multiplicador: {this.state.mult} <br />
                    <input type="button" value="Alterar Multiplicador" onClick={this.updateMult} />
                </p>
            </div>
        )
    }
}

export default App;
